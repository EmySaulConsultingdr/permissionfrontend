const controller = 'permissions';

export default (Axios, baseUrl) => {
    return {
       async getAll() {
            return await Axios.get(`${baseUrl}/${controller}`);
        },
        async get(id) {
            return await Axios.get(`${baseUrl}/${controller}/${id}`);
        },
        async create(params) {
            return await Axios.post(`${baseUrl}/${controller}`, params);
        },
        async update(id, params) {
            return await Axios.put(`${baseUrl}/${controller}/${id}`, params);
        },
        async remove(id) {
            return await Axios.delete(`${baseUrl}/${controller}/${id}`);
        }
    }
}