import Axios from 'axios';
import PermissionService from '../services/PermissionService';
import PermissionTypeService from '../services/PermissionTypeService';
Axios.defaults.headers.common.Accept = "application/json";

export default {
    permissionService: new PermissionService(Axios, window.baseUrl),
    permissionTypeService: new PermissionTypeService(Axios, window.baseUrl)
}