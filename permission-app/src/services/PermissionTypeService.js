const controller = 'permission-types';

export default (Axios, baseUrl) => {
    return {
       async getAll() {
            return await Axios.get(`${baseUrl}/${controller}`);
        }
    }
}