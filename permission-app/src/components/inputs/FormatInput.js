export default {
    props: {
        value: {
            type: [Number, String, Date, Array]
        },
        label: String,
        validFeedBack: String,
        required: Boolean
    },
    data () {
        return {
            currentValue: null,
        }
    },
    updated () {
        this.currentValue = this.value;
    },
    methods: {
        updateValue (value) {
            this.$emit('input', value)
        },
        state() {
            if (this.currentValue == null) {
              return null;
            }
            const isValid = this.currentValue != ''  && !(!/^$/.test(this.currentValue) && !/^[a-z][a-z\s]*$/i.test(this.currentValue))

            this.$emit('state', isValid)
            return isValid;
        },
        invalidFeedback() {
            if ((!/^$/.test(this.currentValue) && !/^[a-z][a-z\s]*$/i.test(this.currentValue))) {
                return 'Formato invalido.'
            }
            return 'Introduzca un formato valido por favor.'
        }
    }
}