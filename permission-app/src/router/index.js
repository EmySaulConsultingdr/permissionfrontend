import Vue from 'vue'
import VueRouter from 'vue-router'
import Permissions from '../views/Permissions.vue'
import CreateOrEdit from '../views/CreateOrEdit.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Permissions',
    component: Permissions
  },
  {
    path: '/permission/create',
    name: 'Create',
    component: CreateOrEdit
  },
  {
    path: '/permission/:id/edit',
    name: 'Edit',
    component: CreateOrEdit
  }
]

const router = new VueRouter({
  routes
})

export default router
