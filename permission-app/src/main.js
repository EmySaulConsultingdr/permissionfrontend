import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'

import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css'
import vuemoment from 'vue-moment'
import moment from 'moment'
import 'moment/locale/es-do'


import { IconsPlugin } from 'bootstrap-vue'


import services from './services/_index.js'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/assets/css/main.css'


Vue.config.productionTip = false

Vue.use(VueAxios, axios)

Vue.use(VueToast, { position: 'top', duration: 2000, type: 'success', color: 'white' })

Vue.use(IconsPlugin)

Vue.use(vuemoment, {
  moment
})

Vue.use({
  install(Vue){
    Object.defineProperty(Vue.prototype,'$services',{
      value:services
    })
  }
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
